from src.restapi import log

from functools import reduce
from typing import List, Any

PER_H = 60


def chunks(size: int, seq: List[Any]) -> List[List[Any]]:
    return [seq[i:i+size] for i in range(0, len(seq), size)]


def stats(group):
    pings = [x["ping"] for x in group]
    successful_pings = list(filter(lambda x: x < 1000, pings))
    return {
        "time": group[0]["time"],
        "min": min(successful_pings),
        "max": max(successful_pings),
        "avg": round(sum(successful_pings) / len(successful_pings), 2),
        "lost": round(1 - len(successful_pings) / len(pings), 2)
    }


def get(offset: int = 0, count: int = 240, start: int = None, end: int = None):
    pings = log.get(offset=offset * PER_H, count=count * PER_H, start=start,
                    end=end)

    pings = chunks(60, pings)
    pings = map(stats, pings)

    return list(pings)
